import time

from behave import *
from page_objects import seleccionamatricula
from page_objects.properties.matricula_locator import Matricula
from selenium.webdriver.support import expected_conditions as ec


@given('que como productor me encuentro en la pantalla de "La Tienda Andina - Herramienta de control de operaciones EKO"')
def step_impl(context):
    title = context.driver.title
    assert title == "Eko"

@when('presiono sobre un registro de la grilla')
def step_impl(context):
    context.page = seleccionamatricula.NavegaMenu(context.driver)
    context.page.selecciona_menu()


@then('se abre la pantalla de detalle del registro seleccionado')
def step_impl(context):
    context.page = seleccionamatricula.NavegaMenu(context.driver)
    #result = context.page.reg_seleccionado
    #context.wait.until(ec.visibility_of_element_located(Matricula.REG_SELECCIONADO))
    #context.page2 = seleccionamatricula.NavegaMenu(context.driver)
    time.sleep(4)
    result = context.page.reg_seleccionado()
    #context.driver.switch_to.frame(context.driver, *Matricula.REG_SELECCIONADO)
    assert result == True

@when('presiono cerrar de la pantalla (X)')
def step_impl(context):
    context.page = seleccionamatricula.NavegaMenu(context.driver)
    context.page.cerrar_ventana()


@then('vuelvo a la pantalla de La Tienda Andina')
def step_impl(context):
    context.page = seleccionamatricula.NavegaMenu(context.driver)
    time.sleep(4)
    result = context.page.valida_ventana_registro_abierta()
    assert result == True


@when('presiono sobre el ícono visualizar de un registro de la grilla')
def step_impl(context):
    context.page = seleccionamatricula.NavegaMenu(context.driver)
    result = context.page.reg_seleccionado()
    assert result == True

