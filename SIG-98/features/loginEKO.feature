Feature: Verificar el acceso a la pantalla de La Tienda Andina.

  Scenario:
    Given que como productor me encuentro en la pantalla de "La Tienda Andina - Herramienta de control de operaciones EKO"
    When presiono sobre un registro de la grilla
    Then se abre la pantalla de detalle del registro seleccionado
    When presiono cerrar de la pantalla (X)
    Then vuelvo a la pantalla de La Tienda Andina
    When presiono sobre el ícono visualizar de un registro de la grilla
    Then se abre la pantalla de detalle del registro seleccionado