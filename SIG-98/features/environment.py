import time

import behave.configuration
from behave.model import Scenario
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from page_objects import login
from page_objects.helpers import Helpers


def before_all(context):
    helper = Helpers
    options = Options()
    options.add_argument("--no-sandbox")
    options.add_argument("--start-maximized")
    context.driver = webdriver.Chrome("drivers/chromedriver.exe", options=options)
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    Scenario.continue_after_failed_step = False
    context.driver.get(helper.obtener_parametro('url'))
    time.sleep(10)
    context.page = login.LoginPage(context.driver)
    context.page.login(helper.obtener_parametro('username'), helper.obtener_parametro('password'))


def after_all(context):
    context.driver.close()
    context.driver.quit()
