from page_objects.properties.login_page_locators import LoginPageLocators


class LoginPage(LoginPageLocators):
    def __init__(self, driver):
        self.driver = driver

    def login(self, user, password):
        self.driver.find_element(*LoginPageLocators.INP_USUARIO).send_keys(user)
        self.driver.find_element(*LoginPageLocators.INP_CONTRASENIA).send_keys(password)
        self.driver.find_element(*LoginPageLocators.BTN_INGRESAR).click()
