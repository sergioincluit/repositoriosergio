from selenium.webdriver.common.by import By


class LoginPageLocators:
    INP_USUARIO = (By.NAME, "username")
    INP_CONTRASENIA = (By.NAME, "password")
    BTN_INGRESAR = (By.ID, "kc-login")