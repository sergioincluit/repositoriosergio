from selenium.webdriver.common.by import By


class Matricula:
    BTN_MARKETING = (By.ID, "menu-lateral-li-marketing")
    BTN_TIENDA_ANDINA = (By.ID, "menu-lateral-li-la tienda andina")
    SELECT_PRIMER_REGISTRO_GRILLA = (By.XPATH, "//tbody[@class='p-datatable-tbody']//tr[1]/td[1]")
    RUEDITA = (By.XPATH, "//div[@class='p-progress-spinner']")
    REG_SELECCIONADO = (By.XPATH, "//div[@class='p-field p-grid pl2']")
    BTN_CERRAR_VENTANA = (By.XPATH, "//button[@class='ng-tns-c39-0 p-dialog-header-icon p-dialog-header-close p-link p-ripple ng-star-inserted']")

