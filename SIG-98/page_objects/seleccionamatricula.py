import time

import behave.log_capture
from selenium.webdriver.support.wait import WebDriverWait
from page_objects.properties.matricula_locator import Matricula
from selenium.webdriver.support import expected_conditions as ec
from page_objects.helpers import Helpers


class NavegaMenu(Matricula):
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 10)

    def selecciona_menu(self):
        self.wait.until(ec.visibility_of_element_located(Matricula.BTN_MARKETING))
        self.driver.find_element(*Matricula.BTN_MARKETING).click()
        self.driver.find_element(*Matricula.BTN_TIENDA_ANDINA).click()
        #self.wait.until(ec.visibility_of_element_located(Matricula.SELECT_PRIMER_REGISTRO_GRILLA))
        #time.sleep(10)
        # 1. create a wait object
        wait = WebDriverWait(self.driver, 10)
        # 2. execute a wait statement
        #if self.driver.find_element(*Matricula.SELECT_PRIMER_REGISTRO_GRILLA).is_displayed() == True:
        wait.until(ec.presence_of_element_located(Matricula.RUEDITA))
        # 3. execute a wait statement
        wait.until(ec.invisibility_of_element_located(Matricula.RUEDITA))
        #Helpers.waitForElementToBeGone(self, self.driver,Matricula.RUEDITA,1)
        #self.wait.until(ec.invisibility_of_element(Matricula.RUEDITA))
        #self.wait.until(ec.visibility_of_element_located(Matricula.SELECT_PRIMER_REGISTRO_GRILLA))
        self.driver.find_element(*Matricula.SELECT_PRIMER_REGISTRO_GRILLA).click()

    def reg_seleccionado(self):
        self.wait.until(ec.visibility_of_element_located(Matricula.REG_SELECCIONADO))
        #self.driver.switch_to.frame(*Matricula.REG_SELECCIONADO)
        result = self.driver.find_element(*Matricula.REG_SELECCIONADO).is_enabled()
        return result

    def cerrar_ventana(self):
        self.driver.find_element(*Matricula.BTN_CERRAR_VENTANA).click()


    def valida_ventana_registro_abierta(self):
        if self.driver.find_element(*Matricula.SELECT_PRIMER_REGISTRO_GRILLA).is_displayed() == True:
            self.driver.find_element(*Matricula.SELECT_PRIMER_REGISTRO_GRILLA).click()
            return True
        else:
            return False


